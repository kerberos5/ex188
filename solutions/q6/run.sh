#!/bin/bash
podman run -dit --name acme-wp-backend-ts \
-e MYSQL_DATABASE=wordpress_db \
-e MYSQL_USER=dbuser \
-e MYSQL_PASSWORD=mypass \
-e MYSQL_ROOT_PASSWORD=mypass \
-v ~/acme-wp-backend-ts:/var/lib/mysql \
--network acme-wp-ts \
repo.localdomain:5000/mariadb:latest

echo "wp-database create!"

podman run -dit --name acme-wp-app-ts \
-e WORDPRESS_DB_HOST=acme-wp-backend-ts:3306 \
-e WORDPRESS_DB_NAME=wordpress_db \
-e WORDPRESS_DB_USER=dbuser \
-e WORDPRESS_DB_PASSWORD=mypass \
-v ~/acme-wp-app-ts:/var/www/html:Z \
-p 8008:80 \
--network acme-wp-ts \
repo.localdomain:5000/wordpress:latest

echo "wp-app create!"
echo "URL: http://servera:8008/"