# Istructions

### Q1. 
Start a container with the following parameters:  
use image repo.localdomain:5000/nginx:1.0  
The container must be named **acme-demo-html**  
The container must be detached from the command line  
The container’s port **80** must be mapped to external port **8001**  
Copy folder **~/ex188/q1/custom** in **~/custom**  
Now the container must serve the contents of **~/custom** the container internally serves **/usr/share/nginx/html/index.html**  
The container response at this URL http://servera:8001  
Now run command:  

```
$ date > ~/custom/index.html
```
Refresh the browser and you should see your new contents  

### Q2.
Start a container with the following parameters:  
use image repo.localdomain:5000/nginx:1.0  
The container must be named **acme-demo-nginx**  
The container must be deteched from the command line  
The container’s port **80** must be mapped to external port **8002**  
Copy the directory **~/ex188/q2/html** in "/" inside to container  
Copy **~/ex188/q2/nginx.conf** in **/etc/nginx/nginx.conf** inside to container  
Execute the command **nginx -s reload inside** the running container  
Verify at urls http://servera:8002/  

### Q3.
Injecting Variables to Containers  
Create **2** containers with the image repo.localdomain:5000/nginx:1.0  
The container names must be:  
	**acme-demo-runtime_1**  
	**acme-demo-runtime_2**  
	
Container **acme-demo-runtime_1** is passed the following environment variable:  
	Variable name: **WELCOME_MASSAGE**  
	Variable value: **ACME_Container_1**  
	
Container **acme-demo-runtime_2** is passed the following environment variable:  
	Variable name: **WELCOME_MASSAGE**  
	Variable value: **ACME_Container_2**  

Makes the port **8080** externally available and mapped to the container port **80** for all and each of the containers.  

The containers must coexist.
You can test your work by doing following:  
Open a terminal and start the container **acme-demo-runtime_1**  
Browse to http://servera:8080  
If the container is running properly, you should see in your browser the message:  
**ACME_Container_1**  
perform the same procedure for **acme-demo-runtime_2**  

### Q4.
Update **~/ex188/q4/acme-db/Containerfile.acme-db** with the following parameters:  
The base image is repo.localdomain:5000/mariadb:latest  
Accepts the following build **arguments**:  
```
DB_ROOT_PASSWORD
DB_ACME_DATABASE
```

Pass the following environment variables:  
```
MYSQL_USER=dbuser
MYSQL_PASSWORD=redhat123
MYSQL_ROOT_PASSWORD=$DB_ROOT_PASSWORD
MYSQL_DATABASE=$DB_ACME_DATABASE
```

Copies the file **~/ex188/q4/acme-db/acme.sql** in container folder **/docker-entrypoint-initdb.d**  
Build an image tagged acme-db:latest using the file **~/ex188/q4/acme-db/Containerfile.acme-db** and pass the following build arguments  
	Argument name: **DB_ROOT_PASSWORD** -> Argument value: **acme**  
	Argument name: **DB_ACME_DATABASE** -> Argument value: **acme**  
Run container on port **3306**:**3306** and name **acme-db** use network **acme-net**  
	
Update **~/ex188/q4/acme-db-exporter/Containerfile.acme-db-exporter** with the following parameters:  
The base image is docker.io/kerberos5/mycentos:8.5  
Create folder **/export**  
install mysql tool  
Defines the working directory as **/scripts**  
Copies the script **scripts/exporter.sh** into **/scripts**  
The container starts by executing the script **/scripts/exporter.sh**  

Build an image tagged as:  
	**acme-db-exporter:latest**  

Create local folder:  
	**~/export**  

Run container named **acme-db-exporter** use network **acme-net** and map volume **~/export** in **/export**  
Verify that the dump is created when the container starts  

### Q5.
Run and Manage **multi-container** applications  
Create a network named **acme-wp**  
Create the volume for the database, name it **acme-wp-backend**  
Create another volume for the Wordpress application, name it **acme-wp-app**  

Start the Database container with the following setting:  
using image repo.localdomain:5000/mariadb:latest  
Runs detached from the command line  
Must be named **acme-wp-backend**  
Must attach the volume **acme-wp-backend** to the container at the directory **/var/lib/mysql**  
set this environments:  
```
MYSQL_DATABASE=wordpress_db
MYSQL_USER=dbuser
MYSQL_PASSWORD=mypass
MYSQL_ROOT_PASSWORD=mypass
```
Must attach to the network **acme-wp**  

Start the Wordpress container with the following setting:  
repo.localdomain:5000/wordpress:latest  
Runs detached from the command line  
Must be named **acme-wp-app**  
Must attach the volume **acme-wp-app** to the container at the directory **/var/www/html**  
set this environments:  
```
WORDPRESS_DB_HOST=acme-wp-backend:3306
WORDPRESS_DB_NAME=wordpress_db
WORDPRESS_DB_USER=dbuser
WORDPRESS_DB_PASSWORD=mypass
```
expose on port **8000**:**80**  
Must attach to the network **acme-wp**  
Test it on url: http://servera:8000

### Q6. 
Troubleshot Container
Create a new network:  
	**acme-wp-ts**

Create folder:  
	**~/acme-wp-backend-ts**  
	**~/acme-wp-app-ts**  

Run the script **~/ex188/q6/run.sh**  

Investigate why containers don't start correctly  
Do not modify the **/run.sh** script!  
Test it on url: http://servera:8008  